# Wiki Crawl

Search for any Wikipedia article

# Screenshots

![Random Article](screenshots/1.png)
![Search List](screenshots/2.png)
![Article Detail](screenshots/3.png)
![Saved Articles](screenshots/4.png)

## APK

Download APK [here](WikiCrawlApk.apk)