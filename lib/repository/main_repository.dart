import 'package:wikicrawl/data/database.dart';

class Singleton {
  static Singleton _app;
  static Singleton Instance() {
    if (_app == null) {
      _app = Singleton();
    }
    return _app;
  }

  Singleton() {
    database = new Database();
  }

  Database database;
}
