import 'dart:convert';

import 'package:wikicrawl/api/wiki_api.dart';
import 'package:wikicrawl/data/data.dart';
import 'package:wikicrawl/repository/main_repository.dart';

class DetailsRepository {
  WikiApi api = WikiApi();

  Article article;
  List<ArticleImage> articleImages = List();
  ArticleDetail articleDetails;

  bool isSaved = false;

  Database database;

  DetailsRepository(this.article) {
    database = Singleton.Instance().database;
    if (!api.filterImage(this.article.image))
      articleImages.add(
        ArticleImage(imageUrl: this.article.image, articleId: this.article.id),
      );
  }

  fetch() async {
    isSaved = await database.isArticleSaved(article);
    if (!isSaved) {
      var response = await api.fetchDetails(article: article);
      articleDetails = response["detail"];
      for(var image in response["images"])
        if(!articleImages.contains(image)) articleImages.add(image); 
    } else {
      articleDetails = await database.getArticleDetails(article);
      articleImages = await database.getArticleImages(article);
    }
  }

  save() async {
    if (!isSaved) {
      await database.saveArticle(article, articleDetails, articleImages);
      isSaved = true;
    } else {
      await database.deleteArticle(article);
      isSaved = false;
    }
  }
}
