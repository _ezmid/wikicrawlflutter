import 'package:flutter/material.dart';
import 'package:wikicrawl/data/database.dart';
import 'package:wikicrawl/pages/article_detail.dart';
import 'package:wikicrawl/widgets/card.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'dart:async';
import 'dart:ui' as ui;

class PromotedArticle extends StatelessWidget {
  final Article article;
  final GlobalKey stickyKey;
  const PromotedArticle({Key key, this.article, this.stickyKey})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    Completer<ui.Image> completer = new Completer();
    var image = (article.image != null &&
            !article.image.isEmpty &&
            !article.image.contains('.svg') &&
            !article.image.contains('.tif'))
        ? CachedNetworkImage(
            imageUrl: article.image,
            height: 250,
            imageBuilder: (context, provider) {
              provider.resolve(ImageConfiguration()).addListener(
                  (ImageInfo info, bool _) => completer.complete(info.image));
              return FutureBuilder<ui.Image>(
                  future: completer.future,
                  builder:
                      (BuildContext context, AsyncSnapshot<ui.Image> snapshot) {
                    if (snapshot.hasData) {
                      var aspectRatio =
                          snapshot.data.width / snapshot.data.height;
                      return Stack(
                          fit: StackFit.passthrough,
                          overflow: Overflow.clip,
                          alignment: AlignmentDirectional.bottomCenter,
                          children: [
                            Hero(
                              tag: article.image,
                              child: RawImage(
                                  image: snapshot.data,
                                  width: aspectRatio * 250),
                            ),
                            _buildBottom(
                                width: aspectRatio * 250, height: 125 / 2)
                          ]);
                    } else {
                      return Stack(
                        children: [
                          Container(
                            height: 250,
                            child: Center(
                              child: SizedBox(
                                child: CircularProgressIndicator(
                                  valueColor:
                                      AlwaysStoppedAnimation(Colors.blueGrey),
                                ),
                                height: 40,
                                width: 40,
                              ),
                            ),
                          ),
                          _buildBottom()
                        ],
                      );
                    }
                  });
            })
        : Stack(alignment: AlignmentDirectional.bottomCenter, children: [
            Container(
              height: 250,
              width: double.infinity,
              child: Center(
                child: Icon(
                  Icons.broken_image,
                  size: 80,
                  color: Colors.black38,
                ),
              ),
            ),
            _buildBottom()
          ]);
    return Container(
        child: MCard(
      radius: 15,
      padding: EdgeInsets.zero,
      child: GestureDetector(
          child: ClipRRect(
            borderRadius: new BorderRadius.circular(15.0),
            child: image,
          ),
          onTap: () {
            Navigator.of(context).push(
              MaterialPageRoute(
                builder: (context) => ArticleDetailPage(
                      article: article,
                    ),
              ),
            );
          }),
    ));
  }

  Widget _buildBottom({width: double.infinity, height: 250.0}) {
    return Container(
      width: width,
      height: height,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.bottomCenter,
          end: Alignment.topCenter,
          colors: [Colors.black87, Colors.transparent],
        ),
      ),
      alignment: AlignmentDirectional.bottomStart,
      padding: EdgeInsets.all(16),
      child: Text(
        article.title,
        style: TextStyle(
            fontSize: 18, fontWeight: FontWeight.w300, color: Colors.white),
      ),
    );
  }
}
