import 'package:flutter/material.dart';
import 'package:wikicrawl/data/data.dart';
import 'package:wikicrawl/pages/article_detail.dart';
import 'package:cached_network_image/cached_network_image.dart';

class ArticleListTile extends StatelessWidget {
  final Article article;
  const ArticleListTile({Key key, this.article}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListTile(
        onTap: () {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) => ArticleDetailPage(article: article),
            ),
          );
        },
        leading: (article.image != null &&
                !article.image.isEmpty &&
                !article.image.contains('.svg') &&
                !article.image.contains('.tif'))
            ? Hero(
                tag: article.image,
                child: CachedNetworkImage(
                  imageUrl: article.image,
                  width: 40,
                  placeholder: (context, url) => SizedBox(
                        child: CircularProgressIndicator(
                          valueColor: AlwaysStoppedAnimation(Colors.blueGrey),
                        ),
                        height: 40,
                        width: 40,
                      ),
                  errorWidget: (context, url, error) =>
                      new Icon(Icons.broken_image),
                ),
              )
            : Icon(Icons.broken_image),
        title: Text(article.title),
        subtitle: Text(_getSubtitle()),
      ),
    );
  }

  String _getSubtitle() {
    var content = article.content;
    if (content == null) return '';
    if (article.content.length > 80)
      content = '${article.content.substring(0, 80)}...';
    return content;
  }
}
