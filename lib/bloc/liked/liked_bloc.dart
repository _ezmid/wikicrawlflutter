import 'package:bloc/bloc.dart';
import 'package:wikicrawl/repository/main_repository.dart';
import 'bloc.dart';
import 'package:wikicrawl/data/data.dart';

class LikedBloc extends Bloc<LikedEvent, LikedState> {
  @override
  LikedState get initialState => InitialState();

  Stream<List<Article>> articles;
  Database database;

  LikedBloc() {
    database = Singleton.Instance().database;
  }

  @override
  Stream<LikedState> mapEventToState(
    LikedEvent event,
  ) async* {
    yield Loading();
    articles = database.getArticles();
    yield Loaded();
  }
}
