import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class LikedEvent extends Equatable {
  LikedEvent([List props = const []]) : super(props);
}

class Load extends LikedEvent {}
