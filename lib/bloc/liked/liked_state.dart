import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class LikedState extends Equatable {
  LikedState([List props = const []]) : super(props);
}

class InitialState extends LikedState {}

class Loading extends LikedState {}

class Loaded extends LikedState {}
