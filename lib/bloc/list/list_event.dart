import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class ListEvent extends Equatable {
  ListEvent([List props = const []]) : super(props);
}

class ScrollReached extends ListEvent {
  ScrollReached() : super([]);
}

class SearchInitialized extends ListEvent {
  final String query;
  final String language;
  SearchInitialized(this.query, this.language) : super([query, language]);
}

class LoadMoreArticles extends ListEvent {
  LoadMoreArticles() : super([]);
}
