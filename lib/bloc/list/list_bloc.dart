import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:wikicrawl/repository/list_repository.dart';
import './bloc.dart';

class ListBloc extends Bloc<ListEvent, ListState> {
  @override
  ListState get initialState => Idle();

  ListRepository repository = ListRepository();

  @override
  Stream<ListState> mapEventToState(
    ListEvent event,
  ) async* {
    if (event is SearchInitialized) {
      if (event.query.isEmpty) {
        yield Idle();
        return;
      }
      repository.query = event.query;
      repository.language = event.language;
      yield LoadingArticles();
      repository.offset = 0;
      repository.articles.clear();
      var list = await repository.fetchArticles(event.query, event.language);
      repository.articles.addAll(list);
      if (repository.articles.length > 0)
        yield ArticleList();
      else {
        yield NothingFound();
        await Future.delayed(Duration(seconds: 2));
        yield Idle();
      }
    }
    if (event is LoadMoreArticles) {
      yield LoadingMoreArticles();
      repository.offset = repository.offset + repository.limit;
      var list =
          await repository.fetchArticles(repository.query, repository.language);
      if (list.length == 0)
        yield NoMoreArticles();
      else {
        repository.articles.addAll(list);
        yield ArticleList();
      }
    }
  }
}
