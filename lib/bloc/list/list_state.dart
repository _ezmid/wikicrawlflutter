import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:wikicrawl/data/data.dart';

@immutable
abstract class ListState extends Equatable {
  ListState([List props = const []]) : super(props);
}

class InitialListState extends ListState {}

class LoadingArticles extends ListState {}

class LoadingMoreArticles extends ListState {}

class NoMoreArticles extends ListState {}

class Idle extends ListState {}

class NothingFound extends ListState {}

class ArticleList extends ListState {
  List<Article> articles;
  ArticleList([this.articles]) : super(articles);
}
