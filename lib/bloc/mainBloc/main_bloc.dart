import 'package:bloc/bloc.dart';
import 'bloc.dart';

class MainBloc extends Bloc<MainEvent, MainState> {
  @override
  MainState get initialState => Discover();

  @override
  Stream<MainState> mapEventToState(
    MainEvent event,
  ) async* {
    if (event is SwitchToDiscover) {
      yield Discover();
    } else {
      yield Saved();
    }
  }
}
