import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class DetailsState extends Equatable {
  DetailsState([List props = const []]) : super(props);
}

class InitialState extends DetailsState {}

class Fetching extends DetailsState {}

class Saving extends DetailsState {}

class Idle extends DetailsState {}
