import 'package:bloc/bloc.dart';
import 'package:wikicrawl/data/data.dart';
import 'package:wikicrawl/repository/details_repository.dart';
import 'bloc.dart';

class DetailsBloc extends Bloc<DetailsEvent, DetailsState> {
  @override
  DetailsState get initialState => InitialState();

  DetailsRepository repository;

  DetailsBloc(Article article) {
    repository = DetailsRepository(article);
  }

  @override
  Stream<DetailsState> mapEventToState(
    DetailsEvent event,
  ) async* {
    if (event is Fetch) {
      yield Fetching();
      await repository.fetch();
      yield Idle();
    }
    if (event is SavePressed) {
      yield Saving();
      await repository.save();
      yield Idle();
    }
  }
}
