import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class DetailsEvent extends Equatable {
  DetailsEvent([List props = const []]) : super(props);
}

class Fetch extends DetailsEvent {}

class SavePressed extends DetailsEvent {}
