import 'package:moor/moor.dart';
import 'package:moor_flutter/moor_flutter.dart';

import 'data.dart';

part 'database.g.dart';

@UseMoor(tables: [Articles, ArticleDetails, ArticleImages])
class Database extends _$Database {
  Database() : super(FlutterQueryExecutor.inDatabaseFolder(path: 'db.sqlite'));

  Future<bool> isArticleSaved(Article article) async {
    var cursor =
        await (select(articles)..where((t) => t.id.equals(article.id))).get();
    return cursor.length > 0;
  }

  saveArticle(Article article, ArticleDetail articleDetail,
      List<ArticleImage> images) async {
    await into(articles).insert(article);
    await into(articleDetails).insert(articleDetail);
    for (var articleImage in images) {
      await into(articleImages).insert(articleImage);
    }
  }

  deleteArticle(Article article) async {
    await (delete(articleImages)..where((t) => t.articleId.equals(article.id)))
        .go();
    await (delete(articleDetails)..where((t) => t.articleId.equals(article.id)))
        .go();
    await (delete(articles)..where((t) => t.id.equals(article.id))).go();
  }

  Future<ArticleDetail> getArticleDetails(Article article) async {
    var list = await (select(articleDetails)
          ..where((t) => t.articleId.equals(article.id)))
        .get();
    return list.length > 0 ? list[0] : null;
  }

  Future<List<ArticleImage>> getArticleImages(Article article) async {
    return await (select(articleImages)
          ..where((t) => t.articleId.equals(article.id)))
        .get();
  }

  Stream<List<Article>> getArticles() {
    return select(articles).watch();
  }

  @override
  int get schemaVersion => 3;
}
