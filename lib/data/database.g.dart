// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'database.dart';

// **************************************************************************
// MoorGenerator
// **************************************************************************

// ignore_for_file: unnecessary_brace_in_string_interps
class Article extends DataClass implements Insertable<Article> {
  final int id;
  final String title;
  final String content;
  String image;
  final String language;
  Article(
      {@required this.id,
      @required this.title,
      this.content,
      this.image,
      @required this.language});
  factory Article.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final stringType = db.typeSystem.forDartType<String>();
    return Article(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      title:
          stringType.mapFromDatabaseResponse(data['${effectivePrefix}title']),
      content:
          stringType.mapFromDatabaseResponse(data['${effectivePrefix}content']),
      image:
          stringType.mapFromDatabaseResponse(data['${effectivePrefix}image']),
      language: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}language']),
    );
  }
  factory Article.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer = const ValueSerializer.defaults()}) {
    return Article(
      id: serializer.fromJson<int>(json['id']),
      title: serializer.fromJson<String>(json['title']),
      content: serializer.fromJson<String>(json['content']),
      image: serializer.fromJson<String>(json['image']),
      language: serializer.fromJson<String>(json['language']),
    );
  }
  @override
  Map<String, dynamic> toJson(
      {ValueSerializer serializer = const ValueSerializer.defaults()}) {
    return {
      'id': serializer.toJson<int>(id),
      'title': serializer.toJson<String>(title),
      'content': serializer.toJson<String>(content),
      'image': serializer.toJson<String>(image),
      'language': serializer.toJson<String>(language),
    };
  }

  @override
  T createCompanion<T extends UpdateCompanion<Article>>(bool nullToAbsent) {
    return ArticlesCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      title:
          title == null && nullToAbsent ? const Value.absent() : Value(title),
      content: content == null && nullToAbsent
          ? const Value.absent()
          : Value(content),
      image:
          image == null && nullToAbsent ? const Value.absent() : Value(image),
      language: language == null && nullToAbsent
          ? const Value.absent()
          : Value(language),
    ) as T;
  }

  Article copyWith(
          {int id,
          String title,
          String content,
          String image,
          String language}) =>
      Article(
        id: id ?? this.id,
        title: title ?? this.title,
        content: content ?? this.content,
        image: image ?? this.image,
        language: language ?? this.language,
      );
  @override
  String toString() {
    return (StringBuffer('Article(')
          ..write('id: $id, ')
          ..write('title: $title, ')
          ..write('content: $content, ')
          ..write('image: $image, ')
          ..write('language: $language')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      $mrjc(
          $mrjc($mrjc($mrjc(0, id.hashCode), title.hashCode), content.hashCode),
          image.hashCode),
      language.hashCode));
  @override
  bool operator ==(other) =>
      identical(this, other) ||
      (other is Article &&
          other.id == id &&
          other.title == title &&
          other.content == content &&
          other.image == image &&
          other.language == language);
}

class ArticlesCompanion extends UpdateCompanion<Article> {
  final Value<int> id;
  final Value<String> title;
  final Value<String> content;
  final Value<String> image;
  final Value<String> language;
  const ArticlesCompanion({
    this.id = const Value.absent(),
    this.title = const Value.absent(),
    this.content = const Value.absent(),
    this.image = const Value.absent(),
    this.language = const Value.absent(),
  });
}

class $ArticlesTable extends Articles with TableInfo<$ArticlesTable, Article> {
  final GeneratedDatabase _db;
  final String _alias;
  $ArticlesTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn('id', $tableName, false, hasAutoIncrement: true);
  }

  final VerificationMeta _titleMeta = const VerificationMeta('title');
  GeneratedTextColumn _title;
  @override
  GeneratedTextColumn get title => _title ??= _constructTitle();
  GeneratedTextColumn _constructTitle() {
    return GeneratedTextColumn(
      'title',
      $tableName,
      false,
    );
  }

  final VerificationMeta _contentMeta = const VerificationMeta('content');
  GeneratedTextColumn _content;
  @override
  GeneratedTextColumn get content => _content ??= _constructContent();
  GeneratedTextColumn _constructContent() {
    return GeneratedTextColumn(
      'content',
      $tableName,
      true,
    );
  }

  final VerificationMeta _imageMeta = const VerificationMeta('image');
  GeneratedTextColumn _image;
  @override
  GeneratedTextColumn get image => _image ??= _constructImage();
  GeneratedTextColumn _constructImage() {
    return GeneratedTextColumn(
      'image',
      $tableName,
      true,
    );
  }

  final VerificationMeta _languageMeta = const VerificationMeta('language');
  GeneratedTextColumn _language;
  @override
  GeneratedTextColumn get language => _language ??= _constructLanguage();
  GeneratedTextColumn _constructLanguage() {
    return GeneratedTextColumn(
      'language',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns => [id, title, content, image, language];
  @override
  $ArticlesTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'articles';
  @override
  final String actualTableName = 'articles';
  @override
  VerificationContext validateIntegrity(ArticlesCompanion d,
      {bool isInserting = false}) {
    final context = VerificationContext();
    if (d.id.present) {
      context.handle(_idMeta, id.isAcceptableValue(d.id.value, _idMeta));
    } else if (id.isRequired && isInserting) {
      context.missing(_idMeta);
    }
    if (d.title.present) {
      context.handle(
          _titleMeta, title.isAcceptableValue(d.title.value, _titleMeta));
    } else if (title.isRequired && isInserting) {
      context.missing(_titleMeta);
    }
    if (d.content.present) {
      context.handle(_contentMeta,
          content.isAcceptableValue(d.content.value, _contentMeta));
    } else if (content.isRequired && isInserting) {
      context.missing(_contentMeta);
    }
    if (d.image.present) {
      context.handle(
          _imageMeta, image.isAcceptableValue(d.image.value, _imageMeta));
    } else if (image.isRequired && isInserting) {
      context.missing(_imageMeta);
    }
    if (d.language.present) {
      context.handle(_languageMeta,
          language.isAcceptableValue(d.language.value, _languageMeta));
    } else if (language.isRequired && isInserting) {
      context.missing(_languageMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  Article map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return Article.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  Map<String, Variable> entityToSql(ArticlesCompanion d) {
    final map = <String, Variable>{};
    if (d.id.present) {
      map['id'] = Variable<int, IntType>(d.id.value);
    }
    if (d.title.present) {
      map['title'] = Variable<String, StringType>(d.title.value);
    }
    if (d.content.present) {
      map['content'] = Variable<String, StringType>(d.content.value);
    }
    if (d.image.present) {
      map['image'] = Variable<String, StringType>(d.image.value);
    }
    if (d.language.present) {
      map['language'] = Variable<String, StringType>(d.language.value);
    }
    return map;
  }

  @override
  $ArticlesTable createAlias(String alias) {
    return $ArticlesTable(_db, alias);
  }
}

class ArticleDetail extends DataClass implements Insertable<ArticleDetail> {
  final int articleId;
  final String html;
  ArticleDetail({@required this.articleId, this.html});
  factory ArticleDetail.fromData(
      Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final stringType = db.typeSystem.forDartType<String>();
    return ArticleDetail(
      articleId:
          intType.mapFromDatabaseResponse(data['${effectivePrefix}article_id']),
      html: stringType.mapFromDatabaseResponse(data['${effectivePrefix}html']),
    );
  }
  factory ArticleDetail.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer = const ValueSerializer.defaults()}) {
    return ArticleDetail(
      articleId: serializer.fromJson<int>(json['articleId']),
      html: serializer.fromJson<String>(json['html']),
    );
  }
  @override
  Map<String, dynamic> toJson(
      {ValueSerializer serializer = const ValueSerializer.defaults()}) {
    return {
      'articleId': serializer.toJson<int>(articleId),
      'html': serializer.toJson<String>(html),
    };
  }

  @override
  T createCompanion<T extends UpdateCompanion<ArticleDetail>>(
      bool nullToAbsent) {
    return ArticleDetailsCompanion(
      articleId: articleId == null && nullToAbsent
          ? const Value.absent()
          : Value(articleId),
      html: html == null && nullToAbsent ? const Value.absent() : Value(html),
    ) as T;
  }

  ArticleDetail copyWith({int articleId, String html}) => ArticleDetail(
        articleId: articleId ?? this.articleId,
        html: html ?? this.html,
      );
  @override
  String toString() {
    return (StringBuffer('ArticleDetail(')
          ..write('articleId: $articleId, ')
          ..write('html: $html')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc($mrjc(0, articleId.hashCode), html.hashCode));
  @override
  bool operator ==(other) =>
      identical(this, other) ||
      (other is ArticleDetail &&
          other.articleId == articleId &&
          other.html == html);
}

class ArticleDetailsCompanion extends UpdateCompanion<ArticleDetail> {
  final Value<int> articleId;
  final Value<String> html;
  const ArticleDetailsCompanion({
    this.articleId = const Value.absent(),
    this.html = const Value.absent(),
  });
}

class $ArticleDetailsTable extends ArticleDetails
    with TableInfo<$ArticleDetailsTable, ArticleDetail> {
  final GeneratedDatabase _db;
  final String _alias;
  $ArticleDetailsTable(this._db, [this._alias]);
  final VerificationMeta _articleIdMeta = const VerificationMeta('articleId');
  GeneratedIntColumn _articleId;
  @override
  GeneratedIntColumn get articleId => _articleId ??= _constructArticleId();
  GeneratedIntColumn _constructArticleId() {
    return GeneratedIntColumn(
      'article_id',
      $tableName,
      false,
    );
  }

  final VerificationMeta _htmlMeta = const VerificationMeta('html');
  GeneratedTextColumn _html;
  @override
  GeneratedTextColumn get html => _html ??= _constructHtml();
  GeneratedTextColumn _constructHtml() {
    return GeneratedTextColumn(
      'html',
      $tableName,
      true,
    );
  }

  @override
  List<GeneratedColumn> get $columns => [articleId, html];
  @override
  $ArticleDetailsTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'article_details';
  @override
  final String actualTableName = 'article_details';
  @override
  VerificationContext validateIntegrity(ArticleDetailsCompanion d,
      {bool isInserting = false}) {
    final context = VerificationContext();
    if (d.articleId.present) {
      context.handle(_articleIdMeta,
          articleId.isAcceptableValue(d.articleId.value, _articleIdMeta));
    } else if (articleId.isRequired && isInserting) {
      context.missing(_articleIdMeta);
    }
    if (d.html.present) {
      context.handle(
          _htmlMeta, html.isAcceptableValue(d.html.value, _htmlMeta));
    } else if (html.isRequired && isInserting) {
      context.missing(_htmlMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {articleId};
  @override
  ArticleDetail map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return ArticleDetail.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  Map<String, Variable> entityToSql(ArticleDetailsCompanion d) {
    final map = <String, Variable>{};
    if (d.articleId.present) {
      map['article_id'] = Variable<int, IntType>(d.articleId.value);
    }
    if (d.html.present) {
      map['html'] = Variable<String, StringType>(d.html.value);
    }
    return map;
  }

  @override
  $ArticleDetailsTable createAlias(String alias) {
    return $ArticleDetailsTable(_db, alias);
  }
}

class ArticleImage extends DataClass implements Insertable<ArticleImage> {
  final int articleId;
  final String imageUrl;
  ArticleImage({@required this.articleId, @required this.imageUrl});
  factory ArticleImage.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final stringType = db.typeSystem.forDartType<String>();
    return ArticleImage(
      articleId:
          intType.mapFromDatabaseResponse(data['${effectivePrefix}article_id']),
      imageUrl: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}image_url']),
    );
  }
  factory ArticleImage.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer = const ValueSerializer.defaults()}) {
    return ArticleImage(
      articleId: serializer.fromJson<int>(json['articleId']),
      imageUrl: serializer.fromJson<String>(json['imageUrl']),
    );
  }
  @override
  Map<String, dynamic> toJson(
      {ValueSerializer serializer = const ValueSerializer.defaults()}) {
    return {
      'articleId': serializer.toJson<int>(articleId),
      'imageUrl': serializer.toJson<String>(imageUrl),
    };
  }

  @override
  T createCompanion<T extends UpdateCompanion<ArticleImage>>(
      bool nullToAbsent) {
    return ArticleImagesCompanion(
      articleId: articleId == null && nullToAbsent
          ? const Value.absent()
          : Value(articleId),
      imageUrl: imageUrl == null && nullToAbsent
          ? const Value.absent()
          : Value(imageUrl),
    ) as T;
  }

  ArticleImage copyWith({int articleId, String imageUrl}) => ArticleImage(
        articleId: articleId ?? this.articleId,
        imageUrl: imageUrl ?? this.imageUrl,
      );
  @override
  String toString() {
    return (StringBuffer('ArticleImage(')
          ..write('articleId: $articleId, ')
          ..write('imageUrl: $imageUrl')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode =>
      $mrjf($mrjc($mrjc(0, articleId.hashCode), imageUrl.hashCode));
  @override
  bool operator ==(other) =>
      identical(this, other) ||
      (other is ArticleImage &&
          other.articleId == articleId &&
          other.imageUrl == imageUrl);
}

class ArticleImagesCompanion extends UpdateCompanion<ArticleImage> {
  final Value<int> articleId;
  final Value<String> imageUrl;
  const ArticleImagesCompanion({
    this.articleId = const Value.absent(),
    this.imageUrl = const Value.absent(),
  });
}

class $ArticleImagesTable extends ArticleImages
    with TableInfo<$ArticleImagesTable, ArticleImage> {
  final GeneratedDatabase _db;
  final String _alias;
  $ArticleImagesTable(this._db, [this._alias]);
  final VerificationMeta _articleIdMeta = const VerificationMeta('articleId');
  GeneratedIntColumn _articleId;
  @override
  GeneratedIntColumn get articleId => _articleId ??= _constructArticleId();
  GeneratedIntColumn _constructArticleId() {
    return GeneratedIntColumn(
      'article_id',
      $tableName,
      false,
    );
  }

  final VerificationMeta _imageUrlMeta = const VerificationMeta('imageUrl');
  GeneratedTextColumn _imageUrl;
  @override
  GeneratedTextColumn get imageUrl => _imageUrl ??= _constructImageUrl();
  GeneratedTextColumn _constructImageUrl() {
    return GeneratedTextColumn(
      'image_url',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns => [articleId, imageUrl];
  @override
  $ArticleImagesTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'article_images';
  @override
  final String actualTableName = 'article_images';
  @override
  VerificationContext validateIntegrity(ArticleImagesCompanion d,
      {bool isInserting = false}) {
    final context = VerificationContext();
    if (d.articleId.present) {
      context.handle(_articleIdMeta,
          articleId.isAcceptableValue(d.articleId.value, _articleIdMeta));
    } else if (articleId.isRequired && isInserting) {
      context.missing(_articleIdMeta);
    }
    if (d.imageUrl.present) {
      context.handle(_imageUrlMeta,
          imageUrl.isAcceptableValue(d.imageUrl.value, _imageUrlMeta));
    } else if (imageUrl.isRequired && isInserting) {
      context.missing(_imageUrlMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => <GeneratedColumn>{};
  @override
  ArticleImage map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return ArticleImage.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  Map<String, Variable> entityToSql(ArticleImagesCompanion d) {
    final map = <String, Variable>{};
    if (d.articleId.present) {
      map['article_id'] = Variable<int, IntType>(d.articleId.value);
    }
    if (d.imageUrl.present) {
      map['image_url'] = Variable<String, StringType>(d.imageUrl.value);
    }
    return map;
  }

  @override
  $ArticleImagesTable createAlias(String alias) {
    return $ArticleImagesTable(_db, alias);
  }
}

abstract class _$Database extends GeneratedDatabase {
  _$Database(QueryExecutor e) : super(const SqlTypeSystem.withDefaults(), e);
  $ArticlesTable _articles;
  $ArticlesTable get articles => _articles ??= $ArticlesTable(this);
  $ArticleDetailsTable _articleDetails;
  $ArticleDetailsTable get articleDetails =>
      _articleDetails ??= $ArticleDetailsTable(this);
  $ArticleImagesTable _articleImages;
  $ArticleImagesTable get articleImages =>
      _articleImages ??= $ArticleImagesTable(this);
  @override
  List<TableInfo> get allTables => [articles, articleDetails, articleImages];
}
