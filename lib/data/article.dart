import 'package:moor_flutter/moor_flutter.dart';
import 'database.dart';

@DataClassName("Article")
class Articles extends Table {
  IntColumn get id => integer().autoIncrement()();
  TextColumn get title => text()();
  TextColumn get content => text().nullable()();
  TextColumn get image => text().nullable()();
  TextColumn get language => text()();

  @override
  Set<Column> get primaryKey => {id};

  static Article fromResponseJson(json, {language: 'en'}) {
    var lang = json["language"] ?? language;
    return Article(
        id: json['pageid'],
        title: json["title"],
        language: lang,
        content: json["extract"]);
  }
}

@DataClassName("ArticleDetail")
class ArticleDetails extends Table {
  IntColumn get articleId => integer()();
  TextColumn get html => text().nullable()();

  @override
  Set<Column> get primaryKey => {articleId};
}

@DataClassName("ArticleImage")
class ArticleImages extends Table {
  IntColumn get articleId => integer()();
  TextColumn get imageUrl => text()();
}
