import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wikicrawl/bloc/mainBloc/bloc.dart';
import 'package:wikicrawl/pages/discover.dart';
import 'package:wikicrawl/pages/liked.dart';
import 'package:wikicrawl/widgets/card.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'WikiCrawler',
      theme: ThemeData(
        primarySwatch: Colors.blueGrey,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  MainBloc _mainBloc;
  @override
  void initState() {
    _mainBloc = new MainBloc();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<MainBloc>(
      builder: (context) => _mainBloc,
      child: BlocBuilder<MainEvent, MainState>(
          bloc: _mainBloc,
          builder: (context, state) {
            return Scaffold(
              body: Stack(children: [
                _buildPage(state),
                AnimatedPositioned(
                  duration: Duration(milliseconds: 300),
                  bottom: 16,
                  left: 16,
                  right: 16,
                  child: MCard(
                    padding: EdgeInsets.symmetric(vertical: 8, horizontal: 32),
                    radius: 5,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        IconButton(
                            icon: Icon(Icons.explore),
                            color: (state is Discover)
                                ? Colors.blueGrey
                                : Colors.grey,
                            onPressed: () {
                              _mainBloc.dispatch(SwitchToDiscover());
                            }),
                        IconButton(
                          icon: Icon(Icons.favorite),
                          onPressed: () {
                            _mainBloc.dispatch(SwitchToSaved());
                          },
                          color:
                              (state is Saved) ? Colors.blueGrey : Colors.grey,
                        ),
                      ],
                    ),
                  ),
                ),
              ]),
            );
          }),
    );
  }

  Widget _buildPage(MainState state) {
    if (state is Discover) {
      return DiscoverPage();
    } else {
      return LikedPage();
    }
  }
}
