import 'package:flutter/material.dart';
import 'package:wikicrawl/widgets/card.dart';

class SearchWidget extends StatelessWidget {
  final TextEditingController controller;
  final submitted;
  final onChanged;
  final FocusNode focusNode;
  const SearchWidget(
      {Key key,
      this.controller,
      this.submitted,
      this.focusNode,
      this.onChanged})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width - 133,
      child: MCard(
        padding: EdgeInsets.symmetric(vertical: 5, horizontal: 5),
        radius: 5,
        child: TextField(
          focusNode: focusNode,
          textInputAction: TextInputAction.search,
          onChanged: this.onChanged,
          controller: this.controller,
          style: TextStyle(fontSize: 18, fontWeight: FontWeight.w300),
          decoration: InputDecoration(
            border: InputBorder.none,
            hasFloatingPlaceholder: false,
            prefixIcon: Icon(Icons.search),
          ),
          onSubmitted: this.submitted,
        ),
      ),
    );
  }
}
