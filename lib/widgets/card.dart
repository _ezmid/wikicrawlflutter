import 'package:flutter/material.dart';

class MCard extends StatelessWidget {
  final child;
  final Color background;
  final double radius;
  EdgeInsets padding;
  MCard(
      {Key key,
      this.child,
      this.background = Colors.white,
      this.radius = 17,
      this.padding})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (this.padding == null)
      this.padding = EdgeInsets.symmetric(vertical: 10, horizontal: 15);
    return Container(
      child: Stack(
        fit: StackFit.passthrough,
        alignment: AlignmentDirectional.bottomCenter,
        children: [
          Positioned(
            left: 20,
            right: 20,
            bottom: 19,
            top: 30,
            child: Container(
              decoration: BoxDecoration(color: Colors.white, boxShadow: [
                BoxShadow(
                    blurRadius: 30,
                    offset: Offset(0, 5),
                    spreadRadius: 1,
                    color: Colors.black)
              ]),
            ),
          ),
          Container(
            child: Padding(padding: this.padding, child: child),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(this.radius),
                color: this.background),
          )
        ],
      ),
    );
  }
}
