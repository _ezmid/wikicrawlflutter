import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wikicrawl/bloc/liked/bloc.dart';
import 'package:wikicrawl/data/data.dart';
import 'package:wikicrawl/views/article_list_tile.dart';
import 'package:wikicrawl/views/promotedArticle.dart';

class LikedPage extends StatefulWidget {
  LikedPage({Key key}) : super(key: key);

  _LikedPageState createState() => _LikedPageState();
}

class _LikedPageState extends State<LikedPage> {
  LikedBloc _likedBloc;

  @override
  void initState() {
    _likedBloc = new LikedBloc();
    super.initState();
    _likedBloc.dispatch(Load());
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      child: BlocProvider<LikedBloc>(
        builder: (context) => _likedBloc,
        child: BlocBuilder<LikedEvent, LikedState>(
          bloc: _likedBloc,
          builder: (context, state) {
            if (state is Loaded) {
              return StreamBuilder(
                stream: _likedBloc.database.getArticles(),
                initialData: List<Article>(),
                builder: (BuildContext context,
                    AsyncSnapshot<List<Article>> snapshot) {
                  if (snapshot.hasData) {
                    return ListView(
                      padding: EdgeInsets.symmetric(horizontal: 16),
                      children: <Widget>[
                        SizedBox(
                          height: 36,
                        ),
                        Text(
                          'Liked Articles',
                          style: TextStyle(
                              fontSize: 31, fontWeight: FontWeight.w200),
                        ),
                        for (var article in snapshot.data)
                          article.image != null
                              ? Padding(
                                  child: PromotedArticle(article: article),
                                  padding: EdgeInsets.only(bottom: 30),
                                )
                              : ArticleListTile(
                                  article: article,
                                ),
                        if (snapshot.data.length == 0)
                          Container(
                              child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Icon(Icons.favorite_border,
                                        size: 110, color: Colors.grey),
                                    Text("You have no saved articles",
                                        style: TextStyle(
                                            color: Colors.grey,
                                            fontWeight: FontWeight.w300))
                                  ]),
                              height: MediaQuery.of(context).size.height - 120),
                        SizedBox(height: 60)
                      ],
                    );
                  }
                  return Center(
                    child: SizedBox(
                      width: 80,
                      height: 80,
                      child: CircularProgressIndicator(
                        valueColor: AlwaysStoppedAnimation(Colors.blueGrey),
                      ),
                    ),
                  );
                },
              );
            }
            return Center(
              child: SizedBox(
                width: 80,
                height: 80,
                child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation(Colors.blueGrey),
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
