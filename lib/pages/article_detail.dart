import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wikicrawl/bloc/detail/bloc.dart';
import 'package:wikicrawl/data/data.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:wikicrawl/widgets/card.dart';

class ArticleDetailPage extends StatefulWidget {
  Article article;
  ArticleDetailPage({Key key, this.article}) : super(key: key);

  _ArticleDetailPageState createState() => _ArticleDetailPageState();
}

class _ArticleDetailPageState extends State<ArticleDetailPage> {
  DetailsBloc _detailsBloc;
  @override
  void initState() {
    _detailsBloc = DetailsBloc(widget.article);
    super.initState();
    _detailsBloc.dispatch(Fetch());
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<DetailsBloc>(
      builder: (context) => _detailsBloc,
      child: BlocBuilder(
          bloc: _detailsBloc,
          builder: (context, state) {
            return Scaffold(
              body: Stack(
                children: [
                  SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        SizedBox(height: 80),
                        if (_detailsBloc.repository.articleImages.length > 0)
                          CarouselSlider(
                            enableInfiniteScroll:
                                _detailsBloc.repository.articleImages.length >
                                    1,
                            viewportFraction: .6,
                            enlargeCenterPage: true,
                            height: 250.0,
                            items: <Widget>[
                              for (var image
                                  in _detailsBloc.repository.articleImages)
                                Padding(
                                  padding: EdgeInsets.symmetric(
                                      vertical: 30, horizontal: 5),
                                  child: Hero(
                                    tag: image.imageUrl,
                                    child: MCard(
                                      padding: EdgeInsets.zero,
                                      child: ClipRRect(
                                        borderRadius:
                                            new BorderRadius.circular(15.0),
                                        child: CachedNetworkImage(
                                          fit: BoxFit.cover,
                                          imageUrl: image.imageUrl,
                                          placeholder: (context, url) => Center(
                                                child: SizedBox(
                                                  child:
                                                      CircularProgressIndicator(
                                                    valueColor:
                                                        AlwaysStoppedAnimation(
                                                            Colors.blueGrey),
                                                  ),
                                                  height: 40,
                                                  width: 40,
                                                ),
                                              ),
                                          errorWidget: (context, url, error) =>
                                              new Icon(Icons.broken_image),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                            ],
                          ),
                        Padding(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                width: MediaQuery.of(context).size.width - 120,
                                child: Text(
                                  _detailsBloc.repository.article.title,
                                  maxLines: 2,
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(
                                      fontSize: 28,
                                      color: Theme.of(context).primaryColor,
                                      fontWeight: FontWeight.w300),
                                ),
                              ),
                              IconButton(
                                icon: _detailsBloc.repository.isSaved
                                    ? Icon(Icons.favorite, color: Colors.red)
                                    : Icon(Icons.favorite_border,
                                        color: Colors.blueGrey),
                                onPressed: () {
                                  _detailsBloc.dispatch(SavePressed());
                                },
                              )
                            ],
                          ),
                          padding:
                              EdgeInsets.only(left: 32, right: 32, top: 55),
                        ),
                        if (state is Idle || state is Saving)
                          Padding(
                            child: Text(
                                _detailsBloc.repository.articleDetails.html),
                            padding: EdgeInsets.all(32),
                          ),
                        if (state is Fetching)
                          Padding(
                            child: Center(
                              child: SizedBox(
                                width: 80,
                                height: 80,
                                child: CircularProgressIndicator(
                                  valueColor: AlwaysStoppedAnimation(
                                    Colors.blueGrey,
                                  ),
                                ),
                              ),
                            ),
                            padding: EdgeInsets.symmetric(vertical: 50),
                          ),
                      ],
                    ),
                  ),
                  Container(
                    height: 80,
                    child: AppBar(
                      iconTheme: IconThemeData(color: Colors.black),
                      elevation: 0,
                      backgroundColor: Colors.transparent,
                    ),
                  ),
                ],
              ),
            );
          }),
    );
  }
}
