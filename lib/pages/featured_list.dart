import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wikicrawl/bloc/list/bloc.dart';
import 'package:wikicrawl/data/data.dart';
import 'package:wikicrawl/views/promotedArticle.dart';

class FeaturedList extends StatefulWidget {
  FeaturedList({Key key}) : super(key: key);

  _FeaturedListState createState() => _FeaturedListState();
}

class _FeaturedListState extends State<FeaturedList> {
  ListBloc _listBloc;

  List<Article> articleList = List();

  @override
  void initState() {
    _listBloc = BlocProvider.of<ListBloc>(context);
    super.initState();
    Populate();
  }

  Populate() async {
    articleList = await _listBloc.repository.fetchRandom();
    if (mounted) setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 155),
      height: MediaQuery.of(context).size.height - 70,
      child: Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
        Container(
            child: Text(
              'Random article',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.w300),
            ),
            width: double.infinity),
        SizedBox(
          height: 5,
        ),
        if (articleList.length > 0) _buildArticles()[0]
      ]),
    );
  }

  List<Widget> _buildArticles() {
    var index = 0;
    List<Widget> widgets = List();
    for (var article in articleList) {
      widgets.add(PromotedArticle(
        article: article,
        stickyKey: GlobalKey(),
      ));
      index++;
    }
    return widgets;
  }
}
