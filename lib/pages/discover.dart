import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wikicrawl/bloc/list/bloc.dart';
import 'package:wikicrawl/data/database.dart';
import 'package:wikicrawl/pages/featured_list.dart';
import 'package:wikicrawl/views/article_list_tile.dart';
import 'package:wikicrawl/views/promotedArticle.dart';
import 'package:wikicrawl/widgets/card.dart';
import 'package:wikicrawl/widgets/search_widget.dart';

class DiscoverPage extends StatefulWidget {
  DiscoverPage({Key key}) : super(key: key);

  _DiscoverPageState createState() => _DiscoverPageState();
}

class _DiscoverPageState extends State<DiscoverPage> {
  var selectedLanguage;

  GlobalKey stickyKey = GlobalKey();
  GlobalKey listKey = GlobalKey();

  ListBloc _listBloc;

  FocusNode _searchNode = FocusNode();

  bool _isSearchFocused = false;
  bool _lazyLoading = false;

  TextEditingController _searchController = TextEditingController();
  ScrollController _scrollController = ScrollController();

  double _lastScrollPosition = 0;

  @override
  void initState() {
    _listBloc = ListBloc();
    selectedLanguage = 'en';
    _searchNode.addListener(() {
      setState(() {
        //_isSearchFocused = _searchNode.hasFocus;
      });
    });
    _scrollController.addListener(() {
      _listBloc.state.listen((state) {
        if (state is NoMoreArticles) return false;
        if (state is LoadingMoreArticles) _lazyLoading = false;
        if (state is ArticleList && !_lazyLoading) {
          if (_scrollController.offset >=
                  _scrollController.position.maxScrollExtent &&
              !_scrollController.position.outOfRange &&
              (_lastScrollPosition - _scrollController.offset).abs() > 200) {
            _lastScrollPosition = _scrollController.offset;
            _listBloc.dispatch(
              LoadMoreArticles(),
            );
          }
        }
      });
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: BlocProvider<ListBloc>(
        child: BlocBuilder<ListEvent, ListState>(
          bloc: _listBloc,
          builder: (BuildContext context, ListState state) {
            _isSearchFocused = !(state is Idle);
            return Stack(
              children: [
                AnimatedPositioned(
                  curve: Curves.easeIn,
                  duration: Duration(milliseconds: 300),
                  left: 16,
                  right: 16,
                  top: _isSearchFocused ? 0 : 85,
                  child: Container(
                      height: _isSearchFocused
                          ? MediaQuery.of(context).size.height - 0
                          : MediaQuery.of(context).size.height - 55,
                      child: _buildPage(state)),
                ),
                AnimatedPositioned(
                  curve: Curves.easeIn,
                  top: _isSearchFocused ? -170 : 55,
                  left: 16,
                  height: 170,
                  duration: Duration(milliseconds: 200),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Image.asset(
                        'images/logo.png',
                        height: 40,
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Text('The free encyclopedia, that anyone can edit',
                          style: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.w300,
                              color: Colors.black54))
                    ],
                  ),
                ),
                AnimatedPositioned(
                  curve: Curves.easeIn,
                  duration: Duration(milliseconds: 300),
                  left: 16,
                  right: 16,
                  height: 55,
                  top: _isSearchFocused ? 55 : 170,
                  child: Row(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(right: 16),
                        child: SearchWidget(
                          onChanged: (val) {
                            if (val.length == 0) {
                              _listBloc.dispatch(
                                SearchInitialized('', ''),
                              );
                            }
                          },
                          focusNode: _searchNode,
                          controller: _searchController,
                          submitted: (str) {
                            _listBloc.dispatch(
                                SearchInitialized(str, selectedLanguage));
                          },
                        ),
                      ),
                      MCard(
                        radius: 5,
                        child: Padding(
                            child: DropdownButton(
                              icon: Padding(
                                padding: EdgeInsets.only(left: 5),
                                child: Icon(Icons.language),
                              ),
                              elevation: 1,
                              underline: Container(),
                              isDense: true,
                              onChanged: (val) {
                                selectedLanguage = val;
                                setState(() {});
                              },
                              value: selectedLanguage,
                              items: [
                                for (var language
                                    in _listBloc.repository.languages)
                                  DropdownMenuItem(
                                    value: language,
                                    child: Text(
                                      language.toUpperCase(),
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                  )
                              ],
                            ),
                            padding: EdgeInsets.symmetric(vertical: 7)),
                      ),
                    ],
                  ),
                ),
              ],
            );
          },
        ),
        builder: (BuildContext context) => _listBloc,
      ),
    );
  }

  Widget _buildPage(ListState state) {
    if (state is Idle) {
      return FeaturedList();
    } else if (state is LoadingArticles) {
      return Container(
        height: MediaQuery.of(context).size.height,
        child: Center(
          child: SizedBox(
            width: 50,
            height: 50,
            child: CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation(Colors.blueGrey),
            ),
          ),
        ),
      );
    } else if (state is ArticleList ||
        state is LoadingMoreArticles ||
        state is NoMoreArticles) {
      var articleWidgets = _buildArticles(_listBloc.repository.articles);
      if (state is LoadingMoreArticles) {
        articleWidgets.addAll([
          SizedBox(
            height: 30,
          ),
          Center(
            child: SizedBox(
              width: 50,
              height: 50,
              child: CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation(Colors.blueGrey),
              ),
            ),
          ),
          SizedBox(
            height: 80,
          ),
        ]);
      } else if (state is NoMoreArticles) {
        articleWidgets.add(
          Container(
            child: Center(
              child: Text(
                'No more articles to show',
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.w300),
              ),
            ),
            padding: EdgeInsets.only(bottom: 80, top: 30),
          ),
        );
      }
      return Container(
          height: _isSearchFocused
              ? MediaQuery.of(context).size.height - 0
              : MediaQuery.of(context).size.height - 55,
          child: ListView(
            key: listKey,
            children: articleWidgets,
            controller: _scrollController,
          ));
    }
    return Container();
  }

  List<Widget> _buildArticles(List<Article> articles) {
    var index = 0;
    List<Widget> list = List();
    list.add(SizedBox(
      height: 140,
    ));
    for (var article in articles) {
      var widget;
      if (index != 0)
        widget = ArticleListTile(
          article: article,
        );
      else {
        widget = PromotedArticle(
          article: article,
          stickyKey: stickyKey,
        );
      }
      index++;

      list.add(widget);
      list.add(
        SizedBox(
          height: 10,
        ),
      );
    }
    return list;
  }
}
