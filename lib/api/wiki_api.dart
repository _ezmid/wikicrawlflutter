import 'dart:convert';

import 'dart:collection';

import 'package:wikicrawl/data/data.dart';
import 'package:http/http.dart' as http;

class WikiApi {
  String RandomEndpoint =
      'https://en.wikipedia.org/w/api.php?action=query&generator=random&grnnamespace=0&grnlimit=1&prop=info%7Cpageimages&pilicense=any&format=json';
  String ImageEndpoint({String query, String language: 'sk'}) =>
      'http://$language.wikipedia.org/w/api.php?action=query&titles=FILE:$query&prop=imageinfo&iiprop=url&format=json';
  String ArticlesEndpoint({String query, String language: 'sk', int offset}) =>
      'https://$language.wikipedia.org/w/api.php?format=json&action=query&generator=search&gsrsort=relevance&gsrnamespace=0&gsrsearch=$query&gsrlimit=10&gsroffset=$offset&prop=pageimages|extracts&pilimit=max&exintro&explaintext&exsentences=1&exlimit=max&pilicense=any';
  String ArticleDetailEndpoint({Article article}) =>
      'https://${article.language}.wikipedia.org/w/api.php?format=json&action=query&prop=extracts|images&exintro&explaintext&redirects=1&pageids=${article.id}&pilicense=an';

  String FilePath(String name) => name == null
      ? null
      : 'https://en.wikipedia.org/wiki/File:${name.replaceAll("File:", "")}';

  Future<List<Article>> fetchAll(
      {String query, String language: 'en', int offset: 0}) async {
    List<Article> articles = List();
    var response = await http.get(
        ArticlesEndpoint(query: query, language: language, offset: offset));
    var body = json.decode(response.body);
    if (body['query'] == null) return articles;
    articles.addAll(await _parseFromResponse(body, language));

    return articles;
  }

  Future<String> fetchImage({String query, String language: 'en'}) async {
    var response =
        await http.get(ImageEndpoint(query: query, language: language));
    var body = json.decode(response.body);
    LinkedHashMap<String, dynamic> map = body['query']['pages'];
    if (map.length > 0) {
      var image = map.values.toList()[0];
      if (image["imageinfo"] == null ||
          image["imageinfo"].length == 0 ||
          image["imageinfo"][0]["url"] == null) return null;
      var url = image["imageinfo"][0]["url"];
      return url.isEmpty ? null : image["imageinfo"][0]["url"];
    }
    return null;
  }

  Future<dynamic> fetchDetails({Article article}) async {
    List<ArticleImage> images = List();
    var response = await http.get(ArticleDetailEndpoint(article: article));
    var body = json.decode(response.body);
    var articleDetail = body["query"]["pages"]['${article.id}'];
    if (articleDetail["images"] != null)
      for (var image in articleDetail['images']) {
        var img = await fetchImage(
            query: image["title"].replaceAll('File:', ''),
            language: article.language);
        if (filterImage(img)) continue;
        images.add(ArticleImage(imageUrl: img, articleId: article.id));
      }
    return {
      "detail": ArticleDetail(
          articleId: articleDetail['pageid'], html: articleDetail['extract']),
      "images": images
    };
  }

  Future<List<Article>> fetchRandom() async {
    List<Article> articles = List();
    var response = await http.get(RandomEndpoint);
    var body = json.decode(response.body);
    if (body['query'] == null) return articles;
    articles.addAll(await _parseFromResponse(body, 'en'));
    return articles;
  }

  Future<List<Article>> _parseFromResponse(body, language) async {
    List<Article> articles = List();
    LinkedHashMap<String, dynamic> list = body['query']['pages'];
    for (var row in list.values) {
      var article = Articles.fromResponseJson(row, language: language);
      if (row["pageimage"] != null)
        article.image =
            await fetchImage(query: row["pageimage"], language: language);
      articles.add(article);
    }
    return articles;
  }

  bool filterImage(String image) {
    return image == null || image.contains('.tiff') || image.contains('.svg');
  }
}
